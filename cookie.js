var cookie = require('cookie');


module.exports = {
	init : function(options) {
		this.app.use(function(req, res, next) {

			// Request cookie object
			req.cookie = cookie.parse(req.headers['cookie'] || '');

			// Response cookies index
			var cookies = {};

			// Response cookie binding
			res.cookie = function(name, value, options) {
				var setCookie = res.getHeader('Set-Cookie');
				if (typeof setCookie === 'undefined') {
					setCookie = [];
				} else if ( setCookie instanceof Array == false) {
					setCookie = [setCookie];
				}

				res.setHeader('Set-Cookie', setCookie);

				// Overwrite same cookies by index
				var cookieIndex = (typeof cookies[name] === 'undefined') ? setCookie.length : cookies[name];
				setCookie[cookieIndex] = cookie.serialize(name, value, options);
				cookies[name]          = cookieIndex;
			}

			next();
		});
	}
};